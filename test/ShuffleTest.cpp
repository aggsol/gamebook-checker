#include "tiny-unit.hpp"

#include "../src/Parser.hpp"
#include "../src/Shuffle.hpp"
#include <string>
#include <stdexcept>
#include <deque>
#include <map>
#include <random>
#include <iostream>

class ShuffleTest : public tiny::Unit
{
public:
    ShuffleTest()
    : tiny::Unit(__FILE__)
    {
        tiny::Unit::registerTest(&ShuffleTest::minComplex, "minComplex");
        tiny::Unit::registerTest(&ShuffleTest::basic, "basic");
    }

    static void minComplex()
    {
        bodhi::Parser parser("../test/mincomplex.adoc");

        std::map<int, bodhi::Section> sections;
        bool error = parser.parse(sections);

        TINY_ASSERT_OK(not error);

        auto resultMapping = bodhi::createSectionMapping(sections, 123456);

        TINY_ASSERT_EQUAL(sections.size(), resultMapping.size());

        for(auto& s: sections)
        {
             TINY_ASSERT_OK(resultMapping.find(s.first) != resultMapping.end());
        }
    }

    static void basic()
    {
        bodhi::Parser parser("../test/linear.adoc");

        std::map<int, bodhi::Section> sections;
        bool error = parser.parse(sections);

        TINY_ASSERT_OK(not error);

        auto resultMapping = bodhi::createSectionMapping(sections, 1337);

        TINY_ASSERT_EQUAL(sections.size(), 10);
        TINY_ASSERT_EQUAL(resultMapping.size(), 10);

        for(auto& r : resultMapping)
        {
            // std::cout << r.first << " -> " << r.second << "\n";
            TINY_ASSERT_OK(sections.find(r.first) != sections.end());
            TINY_ASSERT_OK(sections.find(r.second) != sections.end());
        }

        for(auto& s: sections)
        {
            TINY_ASSERT_OK(resultMapping.find(s.first) != resultMapping.end());
        }
    }
};

ShuffleTest shuffleTest;