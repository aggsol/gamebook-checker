#include "tiny-unit.hpp"

#include "../src/Parser.hpp"
#include <string>
#include <stdexcept>
#include <iostream>

class ParserTest : public tiny::Unit
{
public:
    ParserTest()
    : tiny::Unit(__FILE__)
    {
        tiny::Unit::registerTest(&ParserTest::selfref, "selfref");
        tiny::Unit::registerTest(&ParserTest::image, "image");
        tiny::Unit::registerTest(&ParserTest::stylesection, "stylesection");
        tiny::Unit::registerTest(&ParserTest::badheader, "badheader");
        tiny::Unit::registerTest(&ParserTest::badref, "badref");
        tiny::Unit::registerTest(&ParserTest::classes, "classes");
    }

    static void selfref()
    {
        bodhi::Parser parser("../test/selfref.adoc");

        std::map<int, bodhi::Section> sections;
        bool error = parser.parse(sections);

        TINY_ASSERT_OK(error);
    }

    static void image()
    {
        bodhi::Parser parser("../test/image.adoc");

        std::map<int, bodhi::Section> sections;
        bool error = parser.parse(sections);

        TINY_ASSERT_OK(error);
    }

    static void stylesection()
    {
        bodhi::Parser parser("../test/stylesection.adoc");

        std::map<int, bodhi::Section> sections;
        bool error = parser.parse(sections);

        TINY_ASSERT_OK(not error);
        TINY_ASSERT_EQUAL(2, sections.size());

        for(auto s: sections)
        {
            TINY_ASSERT_EQUAL(s.first, s.second.m_number);
        }
    }

    static void badheader()
    {
        bodhi::Parser parser("../test/badheading.adoc");

        std::map<int, bodhi::Section> sections;
        bool error = parser.parse(sections);

        TINY_ASSERT_OK(error);
    }

    static void badref()
    {
        bodhi::Parser parser("../test/badreference.adoc");

        std::map<int, bodhi::Section> sections;
        bool error = parser.parse(sections);

        TINY_ASSERT_OK(error);
    }

    static void classes()
    {
        bodhi::Parser parser("../test/classes.adoc");

        std::map<int, bodhi::Section> sections;
        bool error = parser.parse(sections);

        TINY_ASSERT_OK(not error);
        TINY_ASSERT_EQUAL(sections.size(), 3);

        TINY_ASSERT_EQUAL(sections[1].m_classes.size(), 1);
        TINY_ASSERT_OK(sections[1].hasClass("fixed"));

        TINY_ASSERT_EQUAL(sections[2].m_classes.size(), 0);
        TINY_ASSERT_OK(not sections[2].hasClass("fixed"));

        TINY_ASSERT_EQUAL(sections[3].m_classes.size(), 3);
        TINY_ASSERT_OK(sections[3].hasClass("value-2"));
        TINY_ASSERT_OK(sections[3].hasClass("dist-3"));
        TINY_ASSERT_OK(sections[3].hasClass("progress-4"));
    }
};

ParserTest parserTest;
