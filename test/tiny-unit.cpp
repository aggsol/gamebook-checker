/**
* Tiny Unit Test Framework
* Author: Kim <foss@aggsol.de>
*
* ----------------------------------------------------------------------------
* "THE BEER-WARE LICENSE" (Revision 42):
* <foss@aggsol.de> wrote this file. As long as you retain this
* notice you can do whatever you want with this stuff. If we meet some day,
* and you think this stuff is worth it, you can buy me a beer in return KIM
* ----------------------------------------------------------------------------
*/
#include "tiny-unit.hpp"
#include <iostream>

namespace
{
    /**
    * Force the unit vector to be initialzed before registering any units.
    */
    struct TinyUnit
    {
        std::vector< tiny::Unit* > unitTests = {};
    };

    TinyUnit& TINY()
    {
        static TinyUnit tinyUnit;
        return tinyUnit;
    }

    void registerUnit(tiny::Unit* pUnit)
    {
        TINY().unitTests.push_back(pUnit);
    }
}

namespace tiny
{
    /**
    * On construction register this unit to run.
    */
    Unit::Unit(const std::string& name)
    : m_name(name)
    {
        if(name.empty() == true)
        {
            throw std::runtime_error("Unit test name must not be empty");
        }
        registerUnit( this );
    }

    void Unit::registerTest(TestFunc foo, const std::string& n)
    {
        if(n.empty() == true)
        {
            throw std::runtime_error("Test name must not be empty");
        }

        if(foo == nullptr)
        {
            throw std::runtime_error("Test function must not be null");
        }

        TestCase testCase;
        testCase.foo = foo;
        testCase.name = n;

        m_testCases.push_back( testCase );
    }

    /**
     * If parameter 'name' is set then only test with matching name run.
     * Returns 0 if the the unit passed, 1 otherwise.
    */
    unsigned Unit::runTests(const std::string& name)
    {
        unsigned fail = 0;

        if(name.empty())
        {
            std::cout << "Unit Test: " << m_name << "\n";
        }

        for(auto & m_testCase : m_testCases)
        {
            try
            {
                if(name.size() > 0 && name != m_testCase.name)
                {
                    fail = 1;
                    continue;
                }

                m_testCase.foo();
                std::cout << "[ Ok ]  " << m_testCase.name << "\n";
            }
            catch(const TestFailed& ex)
            {
                std::cout << "[FAIL]  " << m_testCase.name << "  "
                          << ex.what() << "\n";
                fail = 1;
            }
            catch(const std::exception& ex)
            {
                std::cout << "[FAIL]  " << m_testCase.name
                          << "  Unexpected exception: " << ex.what() << "\n";
                fail = 1;
            }
            catch(...)
            {
                std::cout << "[FAIL]  " << m_testCase.name
                          << "  Unexpected type thrown!\n";
                fail = 1;
            }
        }

        return fail;
    }

    /**
    * Test if the expression is false, then report the expression, filename and line.
    */
    void assertOk(bool expr, const char* rep, const char* filename, unsigned line)
    {
        if(expr == false)
        {
            std::ostringstream msg;
            msg << filename << ":" << line << ": <" << rep << "> is false.";
            throw TestFailed( msg.str() );
        }
    }

    void handleMissedException(const std::string& type, const char* filename, unsigned line)
    {
        std::ostringstream msg;
        msg << filename << ":" << line
            <<  ": Expected exception " << type << ". Nothing caught.";
        throw tiny::TestFailed( msg.str() );
    }

    /**
    * Loop through all registered units and run their test cases.
    */
    int runUnits(const std::string& name)
    {
        unsigned failedUnits = 0;
        const unsigned numUnits = TINY().unitTests.size();
        for(unsigned i=0; i<numUnits; ++i)
        {
            failedUnits += TINY().unitTests[i]->runTests(name);
        }

        if(name.empty())
        {
            std::cout << (numUnits-failedUnits) << "/" << numUnits << " unit tests passed.\n";
        }

        if(failedUnits == 0) return 0;

        return failedUnits;
    }
}

/**
* Optional first parameter selects matching tests for execution
*/
int main(int argc, char* argv[])
{
    if(argc > 2)
    {
        std::cerr << "Invalid parameters\n";
        return 255;
    }

    std::string name;
    if(argc == 2)
    {
        name.assign(argv[1]);
    }

#ifdef CODE_COVERAGE
    std::cout << "Force exit code 0\n";
    tiny::runUnits(name);
    return 0;
#else
    return tiny::runUnits(name);
#endif
}
