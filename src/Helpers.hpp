/*
 Copyright (C) 2018  Kim HOANG <foss@aggsol.de>
 This file is part of gamebook-checker.

 xollox is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef BODHI_HELPERS_HPP
#define BODHI_HELPERS_HPP

#include <random>

namespace bodhi
{
    /*
    * Returns an int from range [min, max]
    */
    template<class URNG>
    inline int randint(URNG& generator, int min, int max)
    {
        std::uniform_int_distribution<int> distribution(min, max);
        return distribution(generator);
    }

    /*
    *
    * Taken from: https://stackoverflow.com/questions/4643512/replace-substring-with-another-substring-c
    */
    inline std::string replaceSubStr(std::string& str, const std::string& target,
    const std::string& sub)
    {
        if(target == sub || target.empty())
        {
            return str;
        }

        size_t index = 0;
        while (true)
        {
             /* Locate the substring to replace. */
             index = str.find(target, index);
             if (index == std::string::npos)
             {
                 break;
             }

             /* Make the replacement. */
             str.replace(index, target.size(), sub);

             /* Advance index forward so the next iteration doesn't pick it up as well. */
             index += sub.size();
        }
        return str;
    }
}

#endif