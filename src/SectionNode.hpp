/*
 Copyright (C) 2019  Kim HOANG <foss@aggsol.de>
 This file is part of gamebook-checker.

 xollox is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef BODHI_SECTION_NODE_HPP
#define BODHI_SECTION_NODE_HPP

#include <cstdint>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>

namespace bodhi
{
        class SectionNode
        {
        public:
            SectionNode() = default;

            int sectionNum = 0;
            bool isLeaf = false;
            bool showLabels = true;
            std::string location;
            std::vector<std::string> labels;

            enum class Type: std::uint8_t
            {
                Todo = 0,
                Fixed = 1,
                Combat = 2,
                None = 255
            };

            Type getType() const { return m_type; }
            void setType(Type t)
            {
                if(t < m_type)
                {
                    m_type = t;
                    if(m_type == Type::Todo)
                    {
                        m_fillColor = "ff0000";
                    }
                }
            }

            const std::string& getFillColor() const { return m_fillColor; }
            void setFillColor(uint32_t value)
            {
                if(m_fillColor.empty())
                {
                    std::ostringstream fc;
                    uint8_t rgb[3];
                    rgb[0] = ((value) + 255)/2;
                    rgb[1] = ((value >> 8) + 255)/2;
                    rgb[2] = ((value >> 16) + 255)/2;
                    fc      << std::hex << std::setfill('0')
                            << std::setw(2) << static_cast<int>(rgb[0])
                            << std::setw(2) << static_cast<int>(rgb[1])
                            << std::setw(2) << static_cast<int>(rgb[2])
                            << std::dec;

                    m_fillColor = fc.str();
                }
            }

            std::string getNodeString();

        private:
            Type            m_type = Type::None;
            std::string     m_fillColor;
        };
}

#endif