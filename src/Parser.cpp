/*
 Copyright (C) 2018  Kim HOANG <foss@aggsol.de>
 This file is part of gamebook-checker.

 xollox is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "rang.hpp"
#include "Parser.hpp"
#include <cassert>
#include <istream>
#include <sstream>
#include <iostream>

namespace bodhi
{

Parser::Parser(const std::string& path)
{
    m_file.open(path);
    if(not m_file.is_open())
    {
        throw std::runtime_error("Error  : Cannot load " + path);
    }
}

bool Parser::parse(std::map<int, Section>& sections)
{
    std::string line;
    m_lineCounter = 1;
    while(m_file)
    {
        std::getline(m_file, line);
        try
        {
            parseLine(line, sections);
        }
        catch(const std::exception& ex)
        {
            std::cerr << rang::fg::red
            << "Error in line " << m_lineCounter << " : " << ex.what() << "\n"
            << rang::fg::reset;
            return true;
        }
        m_lineCounter++;
    }
    return false;
}

void Parser::parseLine(const std::string& line, std::map<int, Section>& sections)
{
    if(line.empty())
    {
        if(m_currSection >= 0)
        {
            sections.at(m_currSection).m_lines.push_back(line);
        }
        return;
    }

    bool addLine = true;
    State state = State::Start;
    std::string value;
    unsigned counter = 0;

    for(unsigned i=0; i<line.size() && state != State::Done; ++i)
    {
        const char c = line.at(i);
        char peek = 0;
        if(i<line.size()-1)
        {
            peek = line.at(i+1);
        }

        switch(state)
        {
        case State::Start:
            if(i == 0 && c == '/')
            {
                addLine = false;
                state = State::CommentStart;
                m_lastLine = LineType::Other;
            }
            else if(c == '[')
            {
                state = State::DataStart;
            }
            else if(c == '=')
            {
                counter = 1; // already parsed 1
                state = State::SectionTitle;
                m_lastLine = LineType::Section;
            }
            else if(c == '<')
            {
                state = State::ReferenceStart;
            }
            else if(i == 0 && line.substr(0, 6) == "image:")
            {
                throw std::runtime_error("Images are not supported");
            }
            else
            {
                m_lastLine = LineType::Other;
            }
        break;

        case State::ReferenceStart:
            if(c == '<' && peek == '_')
            {
                value.clear();
                state = State::ReferenceSection;
            }
            else // stray '<' is allowed
            {
                value.clear();
                state = State::Start;
            }
        break;

        case State::ReferenceSection:
            if(c == '>')
            {
                if(peek != '>')
                {
                    throw std::runtime_error("Expected '>' closing reference");
                }
                int section = -1;
                try
                {
                    section = std::stoi(value);
                }
                catch(const std::exception& ex)
                {
                    std::ostringstream msg;
                    msg << "Invalid section number. value=" << value;
                    throw std::runtime_error(msg.str());
                }

                assert(section >= 0);
                if(section == m_currSection)
                {
                    throw std::runtime_error("Cannot reference itself");
                }
                sections[m_currSection].m_children.insert(section);
                state = State::Start;

            }
            else if(std::isdigit(c))
            {
                value += c;
            }
            else if(std::isspace(c))
            {
                throw std::runtime_error("Invalid whitespace in section reference");
            }
            else if(c != '_')
            {
                throw std::runtime_error("Invalid section reference");
            }
        break;

        case State::Style:
            if(c == '\n' || c == ' ' || c == '\t' || c == '\r')
            {
                throw std::runtime_error("Invalid style/class");
            }
            else if(c == ']')
            {
                sections[m_currSection].m_classes.insert(value);
                state = State::Start;
            }
            else if(c == '.')
            {
                sections[m_currSection].m_classes.insert(value);
                value.clear();
            }
            else
            {
                value += c;
            }
        break;

        case State::SectionTitle:
            addLine = false;
            if(c == '=')
            {
                counter++;
                if(counter > 3)
                {
                    throw std::runtime_error("Level 2 header expected: ===");
                }
            }
            else if(c == ' ')
            {
                try
                {
                    m_currSection = std::stoi(line.substr(i));
                    if(counter != 3 && m_currSection > 1)
                    {
                        std::cerr << "section=" << m_currSection << "\n";
                        throw std::runtime_error("Level 2 header expected: ===");
                    }
                    sections[m_currSection].m_number = m_currSection;
                    state = State::Done;
                }
                catch(const std::exception& ex)
                {
                    // Section 0 is a special case
                    if(m_currSection != 0) {
                        std::ostringstream msg;
                        msg << "Invalid section title. Expected: " << m_currSection
                        << " Exception: " << ex.what();
                        throw std::runtime_error(msg.str());
                    }
                }
            }
            else
            {
                throw std::runtime_error("Invalid section header");
            }
        break;

        case State::DataStart:
            if(c == '.')
            {
                value.clear();
                state = State::Style;
            }
            else if(c == '[')
            {
                throw std::runtime_error("Old style anchors are not supported anymore");
            }
            else
            {
                throw std::runtime_error("Invalid attribute. Missing '[' or '.'");
            }
        break;

        case State::CommentStart:
            if(c != '/')
            {
                throw std::runtime_error("Invalid comment. Missing /");
            }
            else
            {
                state = State::Done;
            }
        break;

        default:
            std::ostringstream msg;
            msg << "Unhandled character '" << c << "' Parser State:" << static_cast<int>(state);
            throw std::runtime_error(msg.str());
        break;
        }
    }

    if(state == State::SectionTitle)
    {
        throw std::runtime_error("Invalid section header");
    }

    if(state == State::ReferenceSection)
    {
        throw std::runtime_error("Invalid section reference");
    }

    if(addLine == true && m_currSection >= 0)
    {
        sections.at(m_currSection).m_lines.push_back(line);
    }
    return;
}

}
