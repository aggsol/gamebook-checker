/*
 Copyright (C) 2018  Kim HOANG <foss@aggsol.de>
 This file is part of gamebook-checker.

 xollox is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Graph.hpp"
#include "SectionNode.hpp"
#include <array>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <vector>

namespace
{

    // Crc32.cpp
    // Copyright (c) 2011-2016 Stephan Brumme. All rights reserved.
    // see http://create.stephan-brumme.com/disclaimer.html
    //
    /// compute CRC32 (half-byte algorithm)
    uint32_t crc32(const void* data, size_t length, uint32_t previousCrc32)
    {
        uint32_t crc = ~previousCrc32; // same as previousCrc32 ^ 0xFFFFFFFF
        //   const uint8_t* current = (const uint8_t*) data;
        const uint8_t* current = static_cast<const uint8_t*>(data);

        /// look-up table for half-byte, same as crc32Lookup[0][16*i]
        static const uint32_t Crc32Lookup16[16] =
        {
        0x00000000,0x1DB71064,0x3B6E20C8,0x26D930AC,0x76DC4190,0x6B6B51F4,0x4DB26158,0x5005713C,
        0xEDB88320,0xF00F9344,0xD6D6A3E8,0xCB61B38C,0x9B64C2B0,0x86D3D2D4,0xA00AE278,0xBDBDF21C
        };

        while (length-- != 0)
        {
        crc = Crc32Lookup16[(crc ^  *current      ) & 0x0F] ^ (crc >> 4);
        crc = Crc32Lookup16[(crc ^ (*current >> 4)) & 0x0F] ^ (crc >> 4);
        current++;
        }

        return ~crc; // same as crc ^ 0xFFFFFFFF
    }


    std::array<std::string, 8> s_skipLabels = {
        "fixed"
        ,"Combat"
        ,"combat"
        ,"Karma"
        ,"Skill"
        ,"Stamina"
    };

    bool isSkipLabel(const std::string& l)
    {
        for(unsigned i=0; i<s_skipLabels.size(); ++i)
        {
            if(s_skipLabels[i] == l)
            {
                return true;
            }
        }
        return false;
    }

    std::array<std::string, 4> s_suffixes = {
        "add-item-"
        ,"add-weapon-"
        ,"codeword-"
        ,"location-"
    };

    std::string trimLabel(const std::string& l)
    {
        std::string result = l;
        for(unsigned i=0; i<s_suffixes.size(); ++i)
        {
            auto pos = result.find(s_suffixes[i]);
            if(pos != std::string::npos)
            {
                result.erase(pos, s_suffixes[i].size());
            }
        }
        return result;
    }

    std::ostream& writeColor(std::ostream& strm, uint32_t value)
    {
        uint8_t rgb[3];
        rgb[0] = ((value) + 255)/2;
        rgb[1] = ((value >> 8) + 255)/2;
        rgb[2] = ((value >> 16) + 255)/2;
        strm    << std::hex << std::setfill('0')
                << std::setw(2) << static_cast<int>(rgb[0])
                << std::setw(2) << static_cast<int>(rgb[1])
                << std::setw(2) << static_cast<int>(rgb[2])
                << std::dec;

        return strm;
    }
}

namespace bodhi
{
Graph::Graph(const std::string& filename, uint32_t colorSeed, bool lrGraph)
: m_filename(filename)
, m_colorSeed(colorSeed)
, m_lrGraph(lrGraph)
{
    if(filename.empty())
    {
        throw std::runtime_error("Missing dot filename");
    }
}


void Graph::createDotFile(const std::map<int, Section>& sections)
{
    std::map<std::string, uint32_t> locationColorMap;


    std::ofstream dotfile(m_filename);
    if(not dotfile.is_open())
    {
        throw std::runtime_error("Cannot write dot file");
    }
    dotfile << "digraph G {\n";

    if(m_lrGraph)
    {
        dotfile << "    rankdir=LR;\n";
    }

    dotfile << "    forcelabels=true;\n"
            << "    graph [fontname = \"Arial\"];\n"
            << "    edge [fontname = \"Arial\"];\n"
            << "    node [fontname = \"Arial\" style=filled fillcolor=white];\n";

        std::vector<SectionNode> sectionNodes;
        for(const auto& s: sections)
        {
            SectionNode sectionNode;

            const int sectionNum = s.second.m_number;
            sectionNode.sectionNum = sectionNum;

            for(auto& attr: s.second.m_classes)
            {

                if(attr == "TODO" ||  attr == "todo")
                {
                    sectionNode.setType(SectionNode::Type::Todo);
                    continue;
                }

                if(attr.find("location-") != std::string::npos)
                {
                    sectionNode.location = trimLabel(attr);
                    const uint32_t value = crc32(attr.c_str(), attr.size(), m_colorSeed);

                    locationColorMap[sectionNode.location] = value;
                    sectionNode.setFillColor(value);
                    continue;
                }

                if(attr == "Combat" || attr == "combat")
                {
                     sectionNode.setType(SectionNode::Type::Combat);
                    continue;
                }

                if(attr == "fixed")
                {
                    sectionNode.setType(SectionNode::Type::Fixed);
                    continue;
                }

                if(isSkipLabel(attr))
                {
                    continue;
                }

                sectionNode.labels.push_back(attr);
            }

            for(auto& c:  s.second.m_children)
            {
                dotfile << "    " << sectionNum << " -> " << c << ";\n";
            }
            if(s.second.m_children.size() == 0)
            {
                sectionNode.isLeaf = true;
            }

            dotfile << "    " << sectionNode.getNodeString();

            sectionNodes.push_back(sectionNode);
        }

    // begin subgraph
    dotfile << "\nsubgraph legend {\n";
    dotfile << "    { rank=same ";
    for(auto& entry: locationColorMap)
    {

        dotfile << entry.first << " ";
    }
    dotfile << " }\n";

    for(auto& entry: locationColorMap)
    {

        dotfile << "    " << entry.first << "[shape=record label=\"" << entry.first << "\" fillcolor=\"#";
        writeColor(dotfile, entry.second);
        dotfile << "\"";
        dotfile << "]\n";
    }

    dotfile << "}\n";
    // end subgraph

    dotfile << "}\n";
}
}
