/*
 Copyright (C) 2018  Kim HOANG <foss@aggsol.de>
 This file is part of gamebook-checker.

 xollox is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef BODHI_SECTION_HPP
#define BODHI_SECTION_HPP

#include <set>
#include <deque>
#include <string>

namespace bodhi
{
class Section
{
public:

    bool hasClass(const std::string& name) const
    {
        return (m_classes.find(name) != m_classes.end());
    }

    int     m_number = -1;

    std::deque<std::string>     m_lines;
    std::set<int>               m_parents;
    std::set<int>               m_children;
    std::set<std::string>       m_classes;
};
}

#endif