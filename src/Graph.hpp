/*
 Copyright (C) 2018  Kim HOANG <foss@aggsol.de>
 This file is part of gamebook-checker.

 xollox is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef BODHI_GRAPH_HPP
#define BODHI_GRAPH_HPP

#include "Section.hpp"
#include <cstdint>
#include <map>

namespace bodhi
{
    class Graph
    {
    public:
        Graph(const std::string& filename, uint32_t colorSeed, bool lrGraph);
        Graph() = delete;

        void createDotFile(const std::map<int, Section>& sections);

    private:

        std::string     m_filename;
        uint32_t        m_colorSeed;
        bool            m_lrGraph;
    };
}

#endif