/*
 Copyright (C) 2018  Kim HOANG <foss@aggsol.de>
 This file is part of gamebook-checker.

 xollox is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "Writer.hpp"
#include "Helpers.hpp"
#include <cassert>

namespace bodhi
{
    Writer::Writer(bool verbose)
    : m_verbose(verbose)
    {

    }

    void Writer::writeSections(std::map<int, bodhi::Section>& sections,
        const std::map<int, int> resultMapping,
        std::ostream& strm)
    {
        std::map<int, bodhi::Section> final;

        for(auto& s: sections)
        {
            assert(s.first == s.second.m_number);
            const int shuffled = resultMapping.at(s.first);

            if(m_verbose)
            {
                std::cerr << s.first << " -> " << shuffled << "\n";
            }
            s.second.m_number = shuffled;

            // First pass substitution
            std::map<std::string, std::string> placeholders;
            for(auto& c: s.second.m_children)
            {
                std::string before = "<<_";
                before += std::to_string(c);
                before += ">>";

                std::string placeholder = "<<temp-";
                placeholder += std::to_string(c);
                placeholder += "-";
                placeholder += std::to_string(resultMapping.at(c));
                placeholder += ">>";

                std::string after = "<<_";
                after += std::to_string(resultMapping.at(c));
                after += ">>";

                placeholders[placeholder] = after;

                if(m_verbose)
                {
                    std::cerr << "before='" << before << "'\t "
                    << "temp  ='" << placeholder << "'\t "
                    << "after ='" << after << "'\n";
                }

                for(auto& l: s.second.m_lines)
                {
                    l = bodhi::replaceSubStr(l, before, placeholder);
                }
            }

            // Second pass substitution
            for(auto& l: s.second.m_lines)
            {
                for(auto& p: placeholders)
                {
                    l = bodhi::replaceSubStr(l, p.first, p.second);
                }
            }

            final[shuffled] = s.second;
        }

        for(auto& f: final)
        {
            assert(f.first == f.second.m_number);

            if(f.second.m_number > 1)
            {
                strm << "=== " << f.second.m_number << "\n";
            }
            else
            {
                strm << "== " << f.second.m_number << "\n";
            }

            for(auto& l: f.second.m_lines)
            {
                strm << l  << "\n";
            }
        }
    }
}