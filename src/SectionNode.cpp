/*
 Copyright (C) 2019  Kim HOANG <foss@aggsol.de>
 This file is part of gamebook-checker.

 xollox is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "SectionNode.hpp"

#include <algorithm>
#include <sstream>
#include <string>

namespace bodhi
{
    std::string SectionNode::getNodeString()
    {
        std::ostringstream str;

        str << sectionNum;
        if(not m_fillColor.empty())
        {
            str << " [fillcolor=\"#" << m_fillColor << "\"]";
        }

        switch(m_type)
        {
            case Type::Combat:
                str << " [shape=diamond]";
            break;
            case Type::Fixed:
                str << " [shape=box]";
            break;
            default:
            //NOP
            break;
        }

        if(isLeaf)
        {
            str << " [peripheries=2]";
        }

        str << " [label=\"" << sectionNum << "\\n";

        if(showLabels)
        {
            for(auto l: labels)
            {
                std::transform(l.begin(), l.end(), l.begin(), [](char c) {
                    return c == '-' ? ' ' : c;
                });
                str << l << "\\n";
            }
        }

        str << "\"]";
        str << ";\n";

        return str.str();
    }
}