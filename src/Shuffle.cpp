/*
 Copyright (C) 2018  Kim HOANG <foss@aggsol.de>
 This file is part of gamebook-checker.

 xollox is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef BODHI_SHUFFLE_HPP
#define BODHI_SHUFFLE_HPP

#include "rang.hpp"
#include "Shuffle.hpp"
#include "Helpers.hpp"
#include "Section.hpp"

#include <cassert>
#include <iostream>
#include <map>
#include <list>

namespace bodhi
{
    std::map<int, int> createSectionMapping(const std::map<int, Section>& sections, unsigned seed)
    {
        std::map<int, int> resultMapping;
        std::deque<int> numbers;

        for(const auto& s: sections)
        {
            if(not s.second.hasClass("fixed"))
            {
                numbers.push_back(s.first);
            }
            else
            {
                assert(resultMapping.end() == resultMapping.find(s.first));
                resultMapping[s.first] = s.first;
            }
        }

        std::default_random_engine generator(seed);

        const int minDelta = std::max<int>(3, numbers.size()/10);
        const int maxAttempts = std::max<int>(10, numbers.size()/4);

        std::cerr << "Shuffle: minDelta=" << minDelta << " maxAttempts=" << maxAttempts << "\n";

        while(numbers.size() > 1)
        {
            int index = bodhi::randint(generator, 0, numbers.size()-1);
            const int a = numbers.at(index);
            numbers.erase(numbers.begin() + index);

            index = bodhi::randint(generator, 0, numbers.size()-1);
            int b = numbers.at(index);

            int counter = 0;
            while( (abs(a-b) < minDelta) && (counter < maxAttempts))
            {
                index = bodhi::randint(generator, 0, numbers.size()-1);
                b = numbers.at(index);
                counter++;
            }
            if(counter >= maxAttempts)
            {
                std::cerr << rang::fg::yellow
                << "Warning: Could not map section " << a << " farther away\n"
                << rang::fg::reset;
            }

            numbers.erase(numbers.begin() + index);

            assert(a != b);
            assert(resultMapping.find(a) == resultMapping.end());
            assert(resultMapping.find(b) == resultMapping.end());

            resultMapping[a] = b;
            resultMapping[b] = a;
        }

        if(numbers.size() == 1)
        {
            resultMapping[numbers[0]] = numbers[0];
        }

        assert(resultMapping.size() == sections.size());

        for(const auto& s: sections)
        {
            assert(resultMapping.at(s.first) > 0);
        }

        return resultMapping;
    }
}

#endif