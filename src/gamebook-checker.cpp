/*
 Copyright (C) 2018  Kim HOANG <foss@aggsol.de>
 This file is part of gamebook-checker.

 xollox is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "rang.hpp"
#include "CliArguments.hpp"
#include "Graph.hpp"
#include "Writer.hpp"
#include "Parser.hpp"
#include "Shuffle.hpp"

#include <cassert>
#include <iostream>
#include <sstream>
#include <fstream>

#include <deque>
#include <map>
#include <numeric>
#include <set>
#include <string>

namespace
{
    void printUsage()
    {
        std::cout << "Usage: gamebook-checker [OPTIONS]\n"
                  << "Check gamebook sections in Asciidoctor format\n"
                  << "Example: gamebook-checker -i sections.adoc\n"
                  << "\n"
                  << "Options:\n"
                  << "  -i, --input <file>          Input file in Asciidoctor format\n"
                  << "  -o, --output <file>         Output file, default is stdout\n"
                  << "  -s, --seed <number>         Shuffle seed, default: 12345\n"
                  << "  -d, --dot-file <file>       Create a dot file named <file>\n"
                  << "  -c, --color-seed <number>   Color seed for the generated dot file\n"
                  << "\n"
                  << "Flags:\n"
                  << "  -h, --help                  Show this help text\n"
                  << "  -l, --left-to-right         Create left to right graph\n"
                  << "  -m, --mix-sections          Shuffle the sections\n"
                  << "  -n, --no-color              Disable colored output\n"
                  << "  -v, --verbose               Show additional information\n"
                  << "      --version               Show version\n"


                  << std::endl;
    }
}

int main(int argc, char* argv[])
{
    bodhi::CliArguments args(argc, argv);
    if(args.getOpt("", "version"))
    {
        std::cout << "v1.0.0-beta\n";
        return 0;
    }

    if(args.getOpt("h", "help"))
    {
        printUsage();
        return 0;
    }

    const std::string NONE = "<none>";

    auto input = args.getOpt<std::string>("i", "input", NONE);
    auto output = args.getOpt<std::string>("o", "output", "stdout");
    auto seed = args.getOpt<unsigned>("s", "seed", 12345);
    auto mix = args.getOpt("m", "mix-sections");
    auto dot = args.getOpt<std::string>("d", "dot-file", NONE);
    auto verbose = args.getOpt("v", "verbose");
    auto noColor = args.getOpt("n", "no-color");
    auto colorSeed = args.getOpt<uint32_t>("c", "color-seed", 0);
    auto lrGraph = args.getOpt("l", "left-to-right");

    if(args.arguments().size() > 0)
    {
        std::cerr << "Unknown or missing parameter(s): ";
        for(const auto & a: args.arguments())
        {
            std::cerr << a << " ";
        }
        std::cerr << "\n";
        return 107;
    }

    if(noColor)
    {
        rang::setControlMode(rang::control::Off);
    }

    if(mix && dot != NONE)
    {
        std::cerr << rang::fg::red
        << "Error  : Cannot combine --mix-sections and --dot-file\n"
        << rang::fg::reset;
        return 101;
    }

    if(lrGraph && dot == NONE)
    {
        std::cerr << rang::fg::red
        << "Error  : Missing dot file name for graph\n"
        << rang::fg::reset;
        return 102;
    }

    std::map<int, bodhi::Section> sections;

    try
    {
        bodhi::Parser parser(input);

        if(parser.parse(sections))
        {
            return 101;
        }

        for(auto& s: sections)
        {
            // Set parents
            assert(s.first == s.second.m_number);
            const int parent = s.first;
            for(auto child: s.second.m_children)
            {
                auto it = sections.find(child);
                if(it != sections.end())
                {
                    it->second.m_parents.insert(parent);
                }
                else
                {
                    std::cerr << rang::fg::red
                    << "Error  : Missing section: " << child
                    <<  " linked from section: " << parent << "\n"
                    << rang::fg::reset;
                }
            }

            // Append empty line for section padding
            if(s.second.m_lines.back() != "")
            {
                s.second.m_lines.push_back("");
            }
        }

        // Check parents
        int last = -1;
        for(auto& s: sections)
        {
            if(s.first > 1)
            {
                if(s.first - last != 1)
                {
                    std::cerr << rang::fg::yellow
                    << "Warning: Missing section(s) "  << last+1 << " to " << s.first-1 << "\n"
                    << rang::fg::reset;
                }

                if(s.second.m_parents.size() == 0)
                {
                    assert(s.first == s.second.m_number);
                    std::cerr << rang::fg::yellow
                    << "Warning: No reference to section: " << s.first << "\n"
                    << rang::fg::reset;
                }
            }
            last = s.first;
        }

        if(verbose)
        {
            std::cout << "Line count: " << parser.lineCount() << "\n"
                      << "Number of sections: " << sections.size() << "\n";

            for(auto& s: sections)
            {
                std::cout << "Section: " << s.second.m_number
                    << "\t classes=" << s.second.m_classes.size()
                    << "\t lines=" << s.second.m_lines.size()
                    << "\t children=" << s.second.m_children.size()
                    << "\t parents=" << s.second.m_parents.size() << "\n";
            }
        }

        if(dot != NONE)
        {
            bodhi::Graph graph(dot, colorSeed, lrGraph);
            graph.createDotFile(sections);
            return 0;
        }

        if(mix)
        {
            bodhi::Writer writer(verbose);

            auto resultMapping = bodhi::createSectionMapping(sections, seed);

            if(output == "stdout")
            {
                writer.writeSections(sections, resultMapping, std::cout);
            }
            else
            {
                std::ofstream mixFile(output);
                if(not mixFile.is_open())
                {
                    throw std::runtime_error("Cannot open output file.");
                }
                writer.writeSections(sections, resultMapping, mixFile);
            }
        }
    }
    catch(const std::exception& ex)
    {
        std::cerr << rang::fg::red
        << ex.what()
        << rang::fg::reset
        << "\n"
        << "Try -h/--help\n";
    }
    return 0;
}
