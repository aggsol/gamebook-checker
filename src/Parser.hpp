/*
 Copyright (C) 2018  Kim HOANG <foss@aggsol.de>
 This file is part of gamebook-checker.

 xollox is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef BODHI_PARSER_HPP
#define BODHI_PARSER_HPP

#include <map>
#include <fstream>

#include "Section.hpp"

namespace bodhi
{
class Parser
{
public:
    explicit Parser(const std::string& path);

    /*
    * Returns true on error
    */
    bool parse(std::map<int, Section>& sections);

    int lineCount() const { return m_lineCounter; }

private:

    void parseLine(const std::string& line, std::map<int, Section>& sections);

    int m_currSection = -1;

    enum class LineType
    {
        Other = 0,
        Section = 2,
        Image = 3
    };
    LineType m_lastLine = LineType::Other;

    enum class State
    {
        Done = -1,
        Start = 0,
        CommentStart = 1,
        DataStart = 2,
        SectionTitle = 3,
        Style = 4,
        ReferenceStart = 5,
        ReferenceSection = 6
    };

    std::ifstream m_file;
    int m_lineCounter = 0;
};
}

#endif /* BODHI_PARSER_HPP */
