# gamebook checker v0.1.0

Helper tool for gamebooks written in Asciidoc

*Work In Progress*

## Todo

1. Enforce `+` at the end of section lines?
2. Better test cases
3. Better documentation
4. Lint for sematic line break
5. Support for quotes?

## Usage

```
Options:
  -i, --input <file>          Input file in Asciidoctor format
  -o, --output <file>         Output file, default is stdout
  -s, --seed <number>         Shuffle seed, default: 12345
  -d, --dot-file <file>       Create a dot file named <file>
  -c, --color-seed <number>   Color seed for the generated dot file

Flags:
  -h, --help                  Show this help text
  -m, --mix-sections          Shuffle the sections
  -n, --no-color              Disable colored output
  -v, --verbose               Show additional information
      --version               Show version
```

## Build

### Requirements

1. C++14 compiler
2. Asciidoctor (to generate the book)
3. Graphiz dot (to generate graphs)

Clone the repo and enter the repo folder.
```
$ mkdir build
$ cd build
$ cmake ..
$ cmake --build .
```

Tested only on Ubuntu with g++ 7.3.0, 4.8.6 and clang++ (Sorry!)

### Run the example

Check the sections
```
$ ./gamebook-checker --input ../example/sections.adoc
```

Generate a graph
```
$ ./gamebook-checker --dot-file graph.dot --input ../example/sections.adoc
$ dot -Tpng graph.dot -ograph.png
```

Mix sections
```
$ ./gamebook-checker --mix-sections --input ../example/sections.adoc --output mixed.adoc
```

## Format

Subset of [Asciidoc](http://asciidoc.org/) is used to write the gamebook.

### Book

A book is composed from several files. The actual story with sections is in a file names `sections.adoc`. The content files are referenced from `book.adoc` Example structure:

```
book.adoc
-> intro.adoc
-> rules.adoc
-> sections.adoc
-> appendix.adoc
```

### Sections

* Sections are numbered
* Section 0 is optional and can be used as prologue
* Section 1 is always the start
* All sections (exept 0) must be in one file `sections.adoc`
* You get a warning for missing or unreferenced sections

### Graph

gamebook-checker can create a graph from the sections where every node is a section.
The output format is aDOT file.
The nodes differ by shape and color.
The Color is generated from the location data.
Also images are tagged to their nodes.
A legend for the color coded locations is added.

### Metadata

Annotation sections with tags. Example:
```
=== 1
[.TODO.combat]
[.my-tag.another-tag]
[.location-crypta]
```

Tags of sections helps to debug and naviagte the story quickliy with the generated graph. Generic annotations will be used as labels for nodes.

|Tag           |Apperance         |Comment                                           |
|--------------|------------------|--------------------------------------------------|
|.combat       |Hexagon Node      |Section has combat                                |
|.TODO         |Node filled red   |Mark section as unfinished                        |
|.location-xyz |Set a fillcolor   |Color is random but the same for one location     |
|.fixed        |Box node          |Section will no be mixed in, fixed section number |

## How to Build the Book

* Makefile to build your book
* Asciidoctor to create HTML and docbook format
* Pandoc to create DOCX from HTML (Is this a good idea?)
* Calibre to create ePub, Mobi and PDF from HTML
* TODO: How are ebooks created from docbook?

## Best Practices

1. Plan thoroughly before start writing
2. Shuffle sections at the very end before the last edit and starting to layout
3. Fix plot holes asap
4. Write related sections together
5. Write one sentence per line

## Licenses

* gamebook-checker is GPLv3
* [rang](https://github.com/agauniyal/rang) is Public Domain
* tiny-unit is Beerware
* CliArguments is Beerware
